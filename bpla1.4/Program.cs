﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bpla1._4 {
	internal static class Program {
		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		private static void Main() {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			// Вызов диалога создания моделирования подрежима 1.4
			var create = new New1_4();
			create.ShowDialog();
			if (!create.GoNext)
				return; // Отмена моделирования

			// Получаем входные значения и переходим к форме моделирования
			var bplaCount = create.GetRes();
			Application.Run(new SubMode1_4Form(bplaCount));
		}
	}
}
