﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bpla1._4 {
	internal class BpPoint {
		public float X { get; set; }
		public float Y { get; set; }

		public BpPoint(float x, float y) {
			X = x;
			Y = y;
		}
	}

	/// <summary>
	/// [Заготовка]
	/// Класс, хранящий данные о БПЛА, такие как скорость, траектория, цвет, радиус.
	/// </summary>
	class BPLA {
		public float Speed { get; private set; }
		public float Radius { get; private set; }
		public BpPoint Coord { get; private set; }
		public Color Color { get; private set; }
		public BpPoint[] Track { get; set; }

		private static ushort _counter;
		public readonly string Id;

		/// <summary>
		/// Устанавливаем радиус
		/// </summary>
		/// <param name="r">Радиус</param>
		public void SetRadius(float r) {
			Radius = r;
		}

		/// <summary>
		/// Устанавливает БПЛА свои координаты
		/// </summary>
		/// <param name="p">Точка с координатами</param>
		public void SetCoords(BpPoint p) {
			Coord = p;
		}

		/// <summary>
		/// Устанавливает БПЛА свои координаты
		/// </summary>
		/// <param name="x">Координата X</param>
		/// <param name="y">Коордитана Y</param>
		public void SetCoords(float x, float y) {
			Coord = new BpPoint(x, y);
		}

		/// <summary>
		/// Установка БПЛА своего цвета
		/// </summary>
		/// <param name="c">Желаемый цвет</param>
		public void SetColor(Color c) {
			Color = c;
		}

		/// <summary>
		/// Возвращает случайный цвет
		/// </summary>
		/// <returns>Объект типа Color</returns>
		private static Color GetRndColor() {
			var rc = new Random();
			return Color.FromArgb(255, rc.Next(255), rc.Next(255), rc.Next(255));
		}

		public BPLA(BpPoint[] track, float speed, float radius) {
			this.Radius = Radius;
			Color = GetRndColor();
			Track = track;
			Coord = track[0];
			Speed = speed;
			Radius = radius;
			_counter++;
			Id = $@"БПЛА-{_counter}";
		}

		/// <summary>
		/// Сбрасывает счетчик созданных объектов
		/// </summary>
		public static void ResetCount() {
			_counter = 0;
		}

		/// <summary>
		/// Расчет траектории по 3 точкам
		/// </summary>
		private void ProcessTrackData(BpPoint p1, BpPoint p2, BpPoint p3) {
			//Расчеты
		}

		/// <summary>
		/// Очередной шаг движения БПЛА.
		/// Обновляет координаты, используя скорость и данные о траектории
		/// </summary>
		public void Move() {
			//Расчеты и изменение координат
		}
	}
}
