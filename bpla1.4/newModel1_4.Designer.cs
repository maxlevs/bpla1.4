﻿namespace bpla1._4 {
	partial class New1_4 {
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent() {
			/// <summary>
			/// cr_gotoMods
			/// Равносильно DialogResult.Abort
			/// </summary>
			this.cr_gotoMods = new System.Windows.Forms.Button();

			/// <summary>
			/// cr_gotoModel
			/// Равносильно DialogResult.Ok
			/// </summary>
			this.cr_gotoModel = new System.Windows.Forms.Button();
			this.cr_modInfoLabel = new System.Windows.Forms.Label();
			this.cr_bplaCountLabel = new System.Windows.Forms.Label();
			this.cr_bplaCountInput = new System.Windows.Forms.MaskedTextBox();
			this.SuspendLayout();
			// 
			// cr_modInfoLabel
			// 
			this.cr_modInfoLabel.AutoSize = true;
			this.cr_modInfoLabel.Location = new System.Drawing.Point(23, 20);
			this.cr_modInfoLabel.Name = "cr_modInfoLabel";
			this.cr_modInfoLabel.Size = new System.Drawing.Size(170, 19);
			this.cr_modInfoLabel.TabIndex = 0;
			this.cr_modInfoLabel.Text = "Информация о режиме";
			// 
			// cr_gotoMods
			// 
			this.cr_gotoMods.DialogResult = System.Windows.Forms.DialogResult.Abort;
			this.cr_gotoMods.Location = new System.Drawing.Point(242, 198);
			this.cr_gotoMods.Name = "cr_gotoMods";
			this.cr_gotoMods.Size = new System.Drawing.Size(166, 42);
			this.cr_gotoMods.TabIndex = 2;
			this.cr_gotoMods.Text = "К режимам";
			this.cr_gotoMods.UseVisualStyleBackColor = true;
			// 
			// cr_gotoModel
			// 
			this.cr_gotoModel.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.cr_gotoModel.Location = new System.Drawing.Point(27, 198);
			this.cr_gotoModel.Name = "cr_gotoModel";
			this.cr_gotoModel.Size = new System.Drawing.Size(166, 42);
			this.cr_gotoModel.TabIndex = 1;
			this.cr_gotoModel.Text = "Перейти к модели";
			this.cr_gotoModel.UseVisualStyleBackColor = true;
			this.cr_gotoModel.Click += new System.EventHandler(this.cr_gotoModel_Click);
			// 
			// cr_bplaCountLabel
			// 
			this.cr_bplaCountLabel.AutoSize = true;
			this.cr_bplaCountLabel.Location = new System.Drawing.Point(23, 120);
			this.cr_bplaCountLabel.Name = "cr_bplaCountLabel";
			this.cr_bplaCountLabel.Size = new System.Drawing.Size(130, 19);
			this.cr_bplaCountLabel.TabIndex = 3;
			this.cr_bplaCountLabel.Text = "Количество БПЛА";
			// 
			// cr_bplaCountInput
			// 
			this.cr_bplaCountInput.BeepOnError = true;
			this.cr_bplaCountInput.Location = new System.Drawing.Point(242, 117);
			this.cr_bplaCountInput.Mask = "#0";
			this.cr_bplaCountInput.Name = "cr_bplaCountInput";
			this.cr_bplaCountInput.Size = new System.Drawing.Size(166, 27);
			this.cr_bplaCountInput.TabIndex = 0;
			this.cr_bplaCountInput.Enter += new System.EventHandler(this.cr_bplaCountInput_Enter);
			this.cr_bplaCountInput.Validated += new System.EventHandler(this.cr_bplaCountInput_Validated);
			// 
			// New1_4
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(434, 259);
			this.Controls.Add(this.cr_bplaCountInput);
			this.Controls.Add(this.cr_bplaCountLabel);
			this.Controls.Add(this.cr_gotoModel);
			this.Controls.Add(this.cr_gotoMods);
			this.Controls.Add(this.cr_modInfoLabel);
			this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "New1_4";
			this.Text = "Подрежим 1.4";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.new1_4_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label cr_modInfoLabel;
		private System.Windows.Forms.Button cr_gotoMods;
		private System.Windows.Forms.Button cr_gotoModel;
		private System.Windows.Forms.Label cr_bplaCountLabel;
		private System.Windows.Forms.MaskedTextBox cr_bplaCountInput;
	}
}

