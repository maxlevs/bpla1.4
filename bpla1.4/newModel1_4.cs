﻿using System;
using System.Windows.Forms;

namespace bpla1._4 {
	public partial class New1_4 : Form {

		/// <summary>
		/// Ограничения, в пределах которых нужно вводить данные
		/// </summary>
		private const int MinLim = 2;

		/// <summary>
		/// Ограничения, в пределах которых нужно вводить данные
		/// </summary>
		private const int MaxLim = 50;

		/// <summary>
		/// Числовое значение из поля ввода
		/// </summary>
		private uint _result;

		/// <summary>
		/// Указывает на корректность текущих значений в текстовом поле
		/// </summary>
		private bool _valid;

		/// <summary>
		/// Хочет ли пользователь произвести моделирование
		/// </summary>
		public bool GoNext { get; private set; }

		public New1_4() {
			InitializeComponent();
			_result = 0;

			_valid = false;
			GoNext = false;
		}

		/// <summary>
		/// Получить результаты
		/// </summary>
		/// <returns>_result - результаты</returns>
		public uint GetRes() {
			return _result; // Отдаем результаты
		}

		/// <summary>
		/// Обработка нажатия "Перейти к моделированию"
		/// </summary>
		private void cr_gotoModel_Click(object sender, EventArgs e) {
			GoNext = true; // Хочу моделировать
			if (!_valid) {
				MessageBox.Show(
					$@"Количество БПЛА должно быть целым числом в пределах от {MinLim} до {MaxLim} включительно.",
					@"Результаты проверки",
					MessageBoxButtons.OK,
					MessageBoxIcon.Warning
				);
				_result = 0;
			} else
				_result = uint.Parse(cr_bplaCountInput.Text); // Если все правильно, записываем результаты
		}

		/// <summary>
		/// Закрытие формы
		/// </summary>
		private void new1_4_FormClosing(object sender, FormClosingEventArgs e) {
			if (GoNext && !_valid) { // Если хочет моделировать, но испытывает трудности с подбором значений
				GoNext = false;
				e.Cancel = true; // Не даем системе закрыть окно
			}
		}

		/// <summary>
		/// Проверка корректности вводимых значений
		/// </summary>
		private void cr_bplaCountInput_Validated(object sender, EventArgs e) {
			uint res;
			uint.TryParse(cr_bplaCountInput.Text, out res);
			if (res < MinLim || res > MaxLim) 
				_valid = false;
			else 
				_valid = true;
		}

		/// <summary>
		/// Переход к текстовому полю
		/// </summary>
		private void cr_bplaCountInput_Enter(object sender, EventArgs e) {
			_valid = false; // Перед вводом считаем, что значение заранее не подходит
		}
	}
}
