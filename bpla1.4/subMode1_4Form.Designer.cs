﻿namespace bpla1._4 {
	partial class SubMode1_4Form {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.sm_poligon = new System.Windows.Forms.Panel();
			this.label28 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.label32 = new System.Windows.Forms.Label();
			this.label33 = new System.Windows.Forms.Label();
			this.label34 = new System.Windows.Forms.Label();
			this.label35 = new System.Windows.Forms.Label();
			this.label36 = new System.Windows.Forms.Label();
			this.label37 = new System.Windows.Forms.Label();
			this.label38 = new System.Windows.Forms.Label();
			this.label39 = new System.Windows.Forms.Label();
			this.label40 = new System.Windows.Forms.Label();
			this.label41 = new System.Windows.Forms.Label();
			this.label42 = new System.Windows.Forms.Label();
			this.label43 = new System.Windows.Forms.Label();
			this.sm_bplaSelector = new System.Windows.Forms.ComboBox();
			this.sm_next = new System.Windows.Forms.Button();
			this.sm_prev = new System.Windows.Forms.Button();
			this.sm_start = new System.Windows.Forms.Button();
			this.speedMeash = new System.Windows.Forms.Label();
			this.sm_radiusImput = new System.Windows.Forms.MaskedTextBox();
			this.radiusLabel = new System.Windows.Forms.Label();
			this.speedLabel = new System.Windows.Forms.Label();
			this.sm_speedImput = new System.Windows.Forms.MaskedTextBox();
			this.radiusMeash = new System.Windows.Forms.Label();
			this.sm_p1Label = new System.Windows.Forms.Label();
			this.sm_p1X = new System.Windows.Forms.MaskedTextBox();
			this.p1xLabel = new System.Windows.Forms.Label();
			this.p1yLabel = new System.Windows.Forms.Label();
			this.sm_p1Y = new System.Windows.Forms.MaskedTextBox();
			this.sm_settingsBox = new System.Windows.Forms.GroupBox();
			this.sm_p3Y = new System.Windows.Forms.MaskedTextBox();
			this.p3yLabel = new System.Windows.Forms.Label();
			this.p3xLabel = new System.Windows.Forms.Label();
			this.sm_p3X = new System.Windows.Forms.MaskedTextBox();
			this.sm_p3Label = new System.Windows.Forms.Label();
			this.sm_p2Y = new System.Windows.Forms.MaskedTextBox();
			this.p2yLabel = new System.Windows.Forms.Label();
			this.p2xLabel = new System.Windows.Forms.Label();
			this.sm_p2X = new System.Windows.Forms.MaskedTextBox();
			this.sm_p2Label = new System.Windows.Forms.Label();
			this.label44 = new System.Windows.Forms.Label();
			this.label45 = new System.Windows.Forms.Label();
			this.label46 = new System.Windows.Forms.Label();
			this.label47 = new System.Windows.Forms.Label();
			this.label48 = new System.Windows.Forms.Label();
			this.label49 = new System.Windows.Forms.Label();
			this.label50 = new System.Windows.Forms.Label();
			this.sm_settingsBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(637, 2);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 15);
			this.label1.TabIndex = 1;
			this.label1.Text = "1000";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(637, 598);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(39, 15);
			this.label2.TabIndex = 2;
			this.label2.Text = "-1000";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(639, 302);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(14, 15);
			this.label3.TabIndex = 3;
			this.label3.Text = "0";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(638, 451);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(32, 15);
			this.label5.TabIndex = 5;
			this.label5.Text = "-500";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label6.Location = new System.Drawing.Point(637, 331);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(32, 15);
			this.label6.TabIndex = 6;
			this.label6.Text = "-100";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label7.Location = new System.Drawing.Point(637, 361);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(32, 15);
			this.label7.TabIndex = 7;
			this.label7.Text = "-200";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label8.Location = new System.Drawing.Point(637, 391);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(32, 15);
			this.label8.TabIndex = 8;
			this.label8.Text = "-300";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label9.Location = new System.Drawing.Point(638, 421);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(32, 15);
			this.label9.TabIndex = 9;
			this.label9.Text = "-400";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label10.Location = new System.Drawing.Point(638, 482);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(32, 15);
			this.label10.TabIndex = 10;
			this.label10.Text = "-600";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label11.Location = new System.Drawing.Point(637, 511);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(32, 15);
			this.label11.TabIndex = 11;
			this.label11.Text = "-700";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label12.Location = new System.Drawing.Point(637, 571);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(32, 15);
			this.label12.TabIndex = 13;
			this.label12.Text = "-900";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label13.Location = new System.Drawing.Point(638, 542);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(32, 15);
			this.label13.TabIndex = 12;
			this.label13.Text = "-800";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(637, 272);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(28, 15);
			this.label4.TabIndex = 22;
			this.label4.Text = "100";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label14.Location = new System.Drawing.Point(638, 243);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(28, 15);
			this.label14.TabIndex = 21;
			this.label14.Text = "200";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label15.Location = new System.Drawing.Point(637, 212);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(28, 15);
			this.label15.TabIndex = 20;
			this.label15.Text = "300";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label16.Location = new System.Drawing.Point(638, 183);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(28, 15);
			this.label16.TabIndex = 19;
			this.label16.Text = "400";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label17.Location = new System.Drawing.Point(638, 122);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(28, 15);
			this.label17.TabIndex = 18;
			this.label17.Text = "600";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label18.Location = new System.Drawing.Point(637, 92);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(28, 15);
			this.label18.TabIndex = 17;
			this.label18.Text = "700";
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label19.Location = new System.Drawing.Point(637, 62);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(28, 15);
			this.label19.TabIndex = 16;
			this.label19.Text = "800";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label20.Location = new System.Drawing.Point(637, 32);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(28, 15);
			this.label20.TabIndex = 15;
			this.label20.Text = "900";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label21.Location = new System.Drawing.Point(638, 152);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(28, 15);
			this.label21.TabIndex = 14;
			this.label21.Text = "500";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label22.Location = new System.Drawing.Point(612, 618);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(35, 15);
			this.label22.TabIndex = 23;
			this.label22.Text = "1000";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label23.Location = new System.Drawing.Point(5, 618);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(39, 15);
			this.label23.TabIndex = 24;
			this.label23.Text = "-1000";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label24.Location = new System.Drawing.Point(324, 618);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(14, 15);
			this.label24.TabIndex = 25;
			this.label24.Text = "0";
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label25.Location = new System.Drawing.Point(586, 618);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(28, 15);
			this.label25.TabIndex = 26;
			this.label25.Text = "900";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label26.Location = new System.Drawing.Point(525, 618);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(28, 15);
			this.label26.TabIndex = 28;
			this.label26.Text = "700";
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label27.Location = new System.Drawing.Point(556, 618);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(28, 15);
			this.label27.TabIndex = 27;
			this.label27.Text = "800";
			// 
			// sm_poligon
			// 
			this.sm_poligon.Location = new System.Drawing.Point(30, 12);
			this.sm_poligon.Name = "sm_poligon";
			this.sm_poligon.Size = new System.Drawing.Size(601, 601);
			this.sm_poligon.TabIndex = 0;
			this.sm_poligon.Paint += new System.Windows.Forms.PaintEventHandler(this.sm_poligon_Paint);
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label28.Location = new System.Drawing.Point(465, 618);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(28, 15);
			this.label28.TabIndex = 30;
			this.label28.Text = "500";
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label29.Location = new System.Drawing.Point(496, 618);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(28, 15);
			this.label29.TabIndex = 29;
			this.label29.Text = "600";
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label30.Location = new System.Drawing.Point(344, 618);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(28, 15);
			this.label30.TabIndex = 34;
			this.label30.Text = "100";
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label31.Location = new System.Drawing.Point(375, 618);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(28, 15);
			this.label31.TabIndex = 33;
			this.label31.Text = "200";
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label32.Location = new System.Drawing.Point(404, 618);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(28, 15);
			this.label32.TabIndex = 32;
			this.label32.Text = "300";
			// 
			// label33
			// 
			this.label33.AutoSize = true;
			this.label33.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label33.Location = new System.Drawing.Point(435, 618);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(28, 15);
			this.label33.TabIndex = 31;
			this.label33.Text = "400";
			// 
			// label34
			// 
			this.label34.AutoSize = true;
			this.label34.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label34.Location = new System.Drawing.Point(44, 618);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(32, 15);
			this.label34.TabIndex = 43;
			this.label34.Text = "-900";
			// 
			// label35
			// 
			this.label35.AutoSize = true;
			this.label35.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label35.Location = new System.Drawing.Point(73, 618);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(32, 15);
			this.label35.TabIndex = 42;
			this.label35.Text = "-800";
			// 
			// label36
			// 
			this.label36.AutoSize = true;
			this.label36.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label36.Location = new System.Drawing.Point(102, 618);
			this.label36.Name = "label36";
			this.label36.Size = new System.Drawing.Size(32, 15);
			this.label36.TabIndex = 41;
			this.label36.Text = "-700";
			// 
			// label37
			// 
			this.label37.AutoSize = true;
			this.label37.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label37.Location = new System.Drawing.Point(133, 618);
			this.label37.Name = "label37";
			this.label37.Size = new System.Drawing.Size(32, 15);
			this.label37.TabIndex = 40;
			this.label37.Text = "-600";
			// 
			// label38
			// 
			this.label38.AutoSize = true;
			this.label38.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label38.Location = new System.Drawing.Point(163, 618);
			this.label38.Name = "label38";
			this.label38.Size = new System.Drawing.Size(32, 15);
			this.label38.TabIndex = 39;
			this.label38.Text = "-500";
			// 
			// label39
			// 
			this.label39.AutoSize = true;
			this.label39.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label39.Location = new System.Drawing.Point(194, 618);
			this.label39.Name = "label39";
			this.label39.Size = new System.Drawing.Size(32, 15);
			this.label39.TabIndex = 38;
			this.label39.Text = "-400";
			// 
			// label40
			// 
			this.label40.AutoSize = true;
			this.label40.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label40.Location = new System.Drawing.Point(223, 618);
			this.label40.Name = "label40";
			this.label40.Size = new System.Drawing.Size(32, 15);
			this.label40.TabIndex = 37;
			this.label40.Text = "-300";
			// 
			// label41
			// 
			this.label41.AutoSize = true;
			this.label41.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label41.Location = new System.Drawing.Point(254, 618);
			this.label41.Name = "label41";
			this.label41.Size = new System.Drawing.Size(32, 15);
			this.label41.TabIndex = 36;
			this.label41.Text = "-200";
			// 
			// label42
			// 
			this.label42.AutoSize = true;
			this.label42.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label42.Location = new System.Drawing.Point(284, 618);
			this.label42.Name = "label42";
			this.label42.Size = new System.Drawing.Size(32, 15);
			this.label42.TabIndex = 35;
			this.label42.Text = "-100";
			// 
			// label43
			// 
			this.label43.AutoSize = true;
			this.label43.Location = new System.Drawing.Point(695, 49);
			this.label43.Name = "label43";
			this.label43.Size = new System.Drawing.Size(122, 19);
			this.label43.TabIndex = 44;
			this.label43.Text = "Выберете БПЛА:";
			// 
			// sm_bplaSelector
			// 
			this.sm_bplaSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.sm_bplaSelector.FormattingEnabled = true;
			this.sm_bplaSelector.Location = new System.Drawing.Point(826, 46);
			this.sm_bplaSelector.MaxDropDownItems = 10;
			this.sm_bplaSelector.Name = "sm_bplaSelector";
			this.sm_bplaSelector.Size = new System.Drawing.Size(125, 27);
			this.sm_bplaSelector.TabIndex = 46;
			this.sm_bplaSelector.SelectedIndexChanged += new System.EventHandler(this.sm_bplaSelector_SelectedIndexChanged);
			// 
			// sm_next
			// 
			this.sm_next.Location = new System.Drawing.Point(833, 89);
			this.sm_next.Name = "sm_next";
			this.sm_next.Size = new System.Drawing.Size(118, 27);
			this.sm_next.TabIndex = 47;
			this.sm_next.Text = "След.";
			this.sm_next.UseVisualStyleBackColor = true;
			this.sm_next.Click += new System.EventHandler(this.sm_next_Click);
			// 
			// sm_prev
			// 
			this.sm_prev.Location = new System.Drawing.Point(699, 89);
			this.sm_prev.Name = "sm_prev";
			this.sm_prev.Size = new System.Drawing.Size(118, 27);
			this.sm_prev.TabIndex = 48;
			this.sm_prev.Text = "Пред.";
			this.sm_prev.UseVisualStyleBackColor = true;
			this.sm_prev.Click += new System.EventHandler(this.sm_prev_Click);
			// 
			// sm_start
			// 
			this.sm_start.Location = new System.Drawing.Point(699, 565);
			this.sm_start.Name = "sm_start";
			this.sm_start.Size = new System.Drawing.Size(252, 48);
			this.sm_start.TabIndex = 49;
			this.sm_start.Text = "Начать моделирование";
			this.sm_start.UseVisualStyleBackColor = true;
			this.sm_start.Click += new System.EventHandler(this.sm_start_Click);
			// 
			// speedMeash
			// 
			this.speedMeash.AutoSize = true;
			this.speedMeash.Location = new System.Drawing.Point(148, 66);
			this.speedMeash.Name = "speedMeash";
			this.speedMeash.Size = new System.Drawing.Size(33, 19);
			this.speedMeash.TabIndex = 4;
			this.speedMeash.Text = "м/с";
			// 
			// sm_radiusImput
			// 
			this.sm_radiusImput.Location = new System.Drawing.Point(94, 33);
			this.sm_radiusImput.Mask = "90.9";
			this.sm_radiusImput.Name = "sm_radiusImput";
			this.sm_radiusImput.Size = new System.Drawing.Size(51, 27);
			this.sm_radiusImput.TabIndex = 0;
			// 
			// radiusLabel
			// 
			this.radiusLabel.AutoSize = true;
			this.radiusLabel.Location = new System.Drawing.Point(12, 36);
			this.radiusLabel.Name = "radiusLabel";
			this.radiusLabel.Size = new System.Drawing.Size(61, 19);
			this.radiusLabel.TabIndex = 1;
			this.radiusLabel.Text = "Радиус:";
			// 
			// speedLabel
			// 
			this.speedLabel.AutoSize = true;
			this.speedLabel.Location = new System.Drawing.Point(12, 66);
			this.speedLabel.Name = "speedLabel";
			this.speedLabel.Size = new System.Drawing.Size(74, 19);
			this.speedLabel.TabIndex = 2;
			this.speedLabel.Text = "Скорость:";
			// 
			// sm_speedImput
			// 
			this.sm_speedImput.Location = new System.Drawing.Point(94, 64);
			this.sm_speedImput.Mask = "90.999";
			this.sm_speedImput.Name = "sm_speedImput";
			this.sm_speedImput.Size = new System.Drawing.Size(51, 27);
			this.sm_speedImput.TabIndex = 3;
			// 
			// radiusMeash
			// 
			this.radiusMeash.AutoSize = true;
			this.radiusMeash.Location = new System.Drawing.Point(148, 36);
			this.radiusMeash.Name = "radiusMeash";
			this.radiusMeash.Size = new System.Drawing.Size(20, 19);
			this.radiusMeash.TabIndex = 5;
			this.radiusMeash.Text = "м";
			// 
			// sm_p1Label
			// 
			this.sm_p1Label.AutoSize = true;
			this.sm_p1Label.Location = new System.Drawing.Point(40, 108);
			this.sm_p1Label.Name = "sm_p1Label";
			this.sm_p1Label.Size = new System.Drawing.Size(46, 19);
			this.sm_p1Label.TabIndex = 6;
			this.sm_p1Label.Text = "Старт";
			// 
			// sm_p1X
			// 
			this.sm_p1X.Location = new System.Drawing.Point(38, 130);
			this.sm_p1X.Mask = "#9990.999";
			this.sm_p1X.Name = "sm_p1X";
			this.sm_p1X.Size = new System.Drawing.Size(80, 27);
			this.sm_p1X.TabIndex = 7;
			// 
			// p1xLabel
			// 
			this.p1xLabel.AutoSize = true;
			this.p1xLabel.Location = new System.Drawing.Point(15, 133);
			this.p1xLabel.Name = "p1xLabel";
			this.p1xLabel.Size = new System.Drawing.Size(21, 19);
			this.p1xLabel.TabIndex = 8;
			this.p1xLabel.Text = "X:";
			// 
			// p1yLabel
			// 
			this.p1yLabel.AutoSize = true;
			this.p1yLabel.Location = new System.Drawing.Point(15, 163);
			this.p1yLabel.Name = "p1yLabel";
			this.p1yLabel.Size = new System.Drawing.Size(20, 19);
			this.p1yLabel.TabIndex = 9;
			this.p1yLabel.Text = "Y:";
			// 
			// sm_p1Y
			// 
			this.sm_p1Y.Location = new System.Drawing.Point(38, 161);
			this.sm_p1Y.Mask = "#9990.999";
			this.sm_p1Y.Name = "sm_p1Y";
			this.sm_p1Y.Size = new System.Drawing.Size(80, 27);
			this.sm_p1Y.TabIndex = 10;
			// 
			// sm_settingsBox
			// 
			this.sm_settingsBox.Controls.Add(this.label50);
			this.sm_settingsBox.Controls.Add(this.label49);
			this.sm_settingsBox.Controls.Add(this.label48);
			this.sm_settingsBox.Controls.Add(this.label47);
			this.sm_settingsBox.Controls.Add(this.label46);
			this.sm_settingsBox.Controls.Add(this.label45);
			this.sm_settingsBox.Controls.Add(this.sm_p3Y);
			this.sm_settingsBox.Controls.Add(this.p3yLabel);
			this.sm_settingsBox.Controls.Add(this.p3xLabel);
			this.sm_settingsBox.Controls.Add(this.sm_p3X);
			this.sm_settingsBox.Controls.Add(this.sm_p3Label);
			this.sm_settingsBox.Controls.Add(this.sm_p2Y);
			this.sm_settingsBox.Controls.Add(this.p2yLabel);
			this.sm_settingsBox.Controls.Add(this.p2xLabel);
			this.sm_settingsBox.Controls.Add(this.sm_p2X);
			this.sm_settingsBox.Controls.Add(this.sm_p2Label);
			this.sm_settingsBox.Controls.Add(this.sm_p1Y);
			this.sm_settingsBox.Controls.Add(this.p1yLabel);
			this.sm_settingsBox.Controls.Add(this.p1xLabel);
			this.sm_settingsBox.Controls.Add(this.sm_p1X);
			this.sm_settingsBox.Controls.Add(this.sm_p1Label);
			this.sm_settingsBox.Controls.Add(this.radiusMeash);
			this.sm_settingsBox.Controls.Add(this.sm_speedImput);
			this.sm_settingsBox.Controls.Add(this.speedLabel);
			this.sm_settingsBox.Controls.Add(this.radiusLabel);
			this.sm_settingsBox.Controls.Add(this.sm_radiusImput);
			this.sm_settingsBox.Controls.Add(this.speedMeash);
			this.sm_settingsBox.Location = new System.Drawing.Point(699, 131);
			this.sm_settingsBox.Name = "sm_settingsBox";
			this.sm_settingsBox.Size = new System.Drawing.Size(252, 387);
			this.sm_settingsBox.TabIndex = 45;
			this.sm_settingsBox.TabStop = false;
			this.sm_settingsBox.Text = "bplaName";
			// 
			// sm_p3Y
			// 
			this.sm_p3Y.Location = new System.Drawing.Point(38, 347);
			this.sm_p3Y.Mask = "#9990.999";
			this.sm_p3Y.Name = "sm_p3Y";
			this.sm_p3Y.Size = new System.Drawing.Size(80, 27);
			this.sm_p3Y.TabIndex = 25;
			// 
			// p3yLabel
			// 
			this.p3yLabel.AutoSize = true;
			this.p3yLabel.Location = new System.Drawing.Point(15, 349);
			this.p3yLabel.Name = "p3yLabel";
			this.p3yLabel.Size = new System.Drawing.Size(20, 19);
			this.p3yLabel.TabIndex = 24;
			this.p3yLabel.Text = "Y:";
			// 
			// p3xLabel
			// 
			this.p3xLabel.AutoSize = true;
			this.p3xLabel.Location = new System.Drawing.Point(15, 319);
			this.p3xLabel.Name = "p3xLabel";
			this.p3xLabel.Size = new System.Drawing.Size(21, 19);
			this.p3xLabel.TabIndex = 23;
			this.p3xLabel.Text = "X:";
			// 
			// sm_p3X
			// 
			this.sm_p3X.Location = new System.Drawing.Point(38, 316);
			this.sm_p3X.Mask = "#9990.999";
			this.sm_p3X.Name = "sm_p3X";
			this.sm_p3X.Size = new System.Drawing.Size(80, 27);
			this.sm_p3X.TabIndex = 22;
			// 
			// sm_p3Label
			// 
			this.sm_p3Label.AutoSize = true;
			this.sm_p3Label.Location = new System.Drawing.Point(40, 294);
			this.sm_p3Label.Name = "sm_p3Label";
			this.sm_p3Label.Size = new System.Drawing.Size(59, 19);
			this.sm_p3Label.TabIndex = 21;
			this.sm_p3Label.Text = "Точка 3";
			// 
			// sm_p2Y
			// 
			this.sm_p2Y.Location = new System.Drawing.Point(38, 253);
			this.sm_p2Y.Mask = "#9990.999";
			this.sm_p2Y.Name = "sm_p2Y";
			this.sm_p2Y.Size = new System.Drawing.Size(80, 27);
			this.sm_p2Y.TabIndex = 20;
			// 
			// p2yLabel
			// 
			this.p2yLabel.AutoSize = true;
			this.p2yLabel.Location = new System.Drawing.Point(15, 255);
			this.p2yLabel.Name = "p2yLabel";
			this.p2yLabel.Size = new System.Drawing.Size(20, 19);
			this.p2yLabel.TabIndex = 19;
			this.p2yLabel.Text = "Y:";
			// 
			// p2xLabel
			// 
			this.p2xLabel.AutoSize = true;
			this.p2xLabel.Location = new System.Drawing.Point(15, 225);
			this.p2xLabel.Name = "p2xLabel";
			this.p2xLabel.Size = new System.Drawing.Size(21, 19);
			this.p2xLabel.TabIndex = 18;
			this.p2xLabel.Text = "X:";
			// 
			// sm_p2X
			// 
			this.sm_p2X.Location = new System.Drawing.Point(38, 222);
			this.sm_p2X.Mask = "#9990.999";
			this.sm_p2X.Name = "sm_p2X";
			this.sm_p2X.Size = new System.Drawing.Size(80, 27);
			this.sm_p2X.TabIndex = 17;
			// 
			// sm_p2Label
			// 
			this.sm_p2Label.AutoSize = true;
			this.sm_p2Label.Location = new System.Drawing.Point(40, 200);
			this.sm_p2Label.Name = "sm_p2Label";
			this.sm_p2Label.Size = new System.Drawing.Size(59, 19);
			this.sm_p2Label.TabIndex = 16;
			this.sm_p2Label.Text = "Точка 2";
			// 
			// label44
			// 
			this.label44.AutoSize = true;
			this.label44.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label44.Location = new System.Drawing.Point(760, 9);
			this.label44.Name = "label44";
			this.label44.Size = new System.Drawing.Size(122, 29);
			this.label44.TabIndex = 50;
			this.label44.Text = "Настройки";
			// 
			// label45
			// 
			this.label45.AutoSize = true;
			this.label45.Location = new System.Drawing.Point(123, 133);
			this.label45.Name = "label45";
			this.label45.Size = new System.Drawing.Size(20, 19);
			this.label45.TabIndex = 26;
			this.label45.Text = "м";
			// 
			// label46
			// 
			this.label46.AutoSize = true;
			this.label46.Location = new System.Drawing.Point(123, 163);
			this.label46.Name = "label46";
			this.label46.Size = new System.Drawing.Size(20, 19);
			this.label46.TabIndex = 27;
			this.label46.Text = "м";
			// 
			// label47
			// 
			this.label47.AutoSize = true;
			this.label47.Location = new System.Drawing.Point(123, 222);
			this.label47.Name = "label47";
			this.label47.Size = new System.Drawing.Size(20, 19);
			this.label47.TabIndex = 28;
			this.label47.Text = "м";
			// 
			// label48
			// 
			this.label48.AutoSize = true;
			this.label48.Location = new System.Drawing.Point(123, 257);
			this.label48.Name = "label48";
			this.label48.Size = new System.Drawing.Size(20, 19);
			this.label48.TabIndex = 29;
			this.label48.Text = "м";
			// 
			// label49
			// 
			this.label49.AutoSize = true;
			this.label49.Location = new System.Drawing.Point(124, 317);
			this.label49.Name = "label49";
			this.label49.Size = new System.Drawing.Size(20, 19);
			this.label49.TabIndex = 30;
			this.label49.Text = "м";
			// 
			// label50
			// 
			this.label50.AutoSize = true;
			this.label50.Location = new System.Drawing.Point(124, 349);
			this.label50.Name = "label50";
			this.label50.Size = new System.Drawing.Size(20, 19);
			this.label50.TabIndex = 31;
			this.label50.Text = "м";
			// 
			// SubMode1_4Form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(976, 650);
			this.Controls.Add(this.label44);
			this.Controls.Add(this.sm_start);
			this.Controls.Add(this.sm_prev);
			this.Controls.Add(this.sm_next);
			this.Controls.Add(this.sm_bplaSelector);
			this.Controls.Add(this.label43);
			this.Controls.Add(this.label34);
			this.Controls.Add(this.label35);
			this.Controls.Add(this.label36);
			this.Controls.Add(this.label37);
			this.Controls.Add(this.label38);
			this.Controls.Add(this.label39);
			this.Controls.Add(this.label40);
			this.Controls.Add(this.label41);
			this.Controls.Add(this.label42);
			this.Controls.Add(this.label30);
			this.Controls.Add(this.label31);
			this.Controls.Add(this.label32);
			this.Controls.Add(this.label33);
			this.Controls.Add(this.label28);
			this.Controls.Add(this.label29);
			this.Controls.Add(this.label26);
			this.Controls.Add(this.label27);
			this.Controls.Add(this.label25);
			this.Controls.Add(this.label24);
			this.Controls.Add(this.label23);
			this.Controls.Add(this.label22);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.label19);
			this.Controls.Add(this.label20);
			this.Controls.Add(this.label21);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.sm_poligon);
			this.Controls.Add(this.sm_settingsBox);
			this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SubMode1_4Form";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.sm_settingsBox.ResumeLayout(false);
			this.sm_settingsBox.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Panel sm_poligon;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.ComboBox sm_bplaSelector;
		private System.Windows.Forms.Button sm_next;
		private System.Windows.Forms.Button sm_prev;
		private System.Windows.Forms.Button sm_start;
		private System.Windows.Forms.Label speedMeash;
		private System.Windows.Forms.MaskedTextBox sm_radiusImput;
		private System.Windows.Forms.Label radiusLabel;
		private System.Windows.Forms.Label speedLabel;
		private System.Windows.Forms.MaskedTextBox sm_speedImput;
		private System.Windows.Forms.Label radiusMeash;
		private System.Windows.Forms.Label sm_p1Label;
		private System.Windows.Forms.MaskedTextBox sm_p1X;
		private System.Windows.Forms.Label p1xLabel;
		private System.Windows.Forms.Label p1yLabel;
		private System.Windows.Forms.MaskedTextBox sm_p1Y;
		private System.Windows.Forms.GroupBox sm_settingsBox;
		private System.Windows.Forms.MaskedTextBox sm_p3Y;
		private System.Windows.Forms.Label p3yLabel;
		private System.Windows.Forms.Label p3xLabel;
		private System.Windows.Forms.MaskedTextBox sm_p3X;
		private System.Windows.Forms.Label sm_p3Label;
		private System.Windows.Forms.MaskedTextBox sm_p2Y;
		private System.Windows.Forms.Label p2yLabel;
		private System.Windows.Forms.Label p2xLabel;
		private System.Windows.Forms.MaskedTextBox sm_p2X;
		private System.Windows.Forms.Label sm_p2Label;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label45;
	}
}