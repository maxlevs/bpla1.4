﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bpla1._4 {
	public partial class SubMode1_4Form : Form {
		private readonly Point _sp = new Point(-1000, 1000); // Двух точек (прямоугольника) полигона в условных единицах (метры?)
		private readonly Point _ep = new Point(1000, -1000);

		private readonly float _dX = 100; // Шаг сетки
		private readonly float _dY = 100;
		
		private readonly float _scaleX; // Отношение реальной длины к пиксельной
		private readonly float _scaleY;
		
		private readonly int _lenX; //Реальные размеры полигона
		private readonly int _lenY;

		/// <summary>
		/// Список учавствующих в моделирование БПЛА
		/// </summary>
		private List<BPLA> _bplas;
		/// <summary>
		/// Список вводимой пользователем информации
		/// </summary>
		private readonly List<DataUnit> _userData;
		/// <summary>
		/// Текущий настраиваемый БПЛА
		/// </summary>
		private int _cur;

		public SubMode1_4Form(uint bplaCount) {
			InitializeComponent();
			//Реальные размеры
			_lenX = sm_poligon.Width  - 1;
			_lenY = sm_poligon.Height - 1;

			//Увеличение до виртуальных размеров
			_scaleX = (float) _lenX / Math.Abs(_ep.X - _sp.X);
			_scaleY = (float) _lenY / Math.Abs(_ep.Y - _sp.Y);

			//Шаг сетки
			_dX *= _scaleX; _dY *= _scaleY;

			BPLA.ResetCount();

			//Промежуточное хранилище
			_userData = new List<DataUnit>();
			for (int i = 1; i <= bplaCount; ++i) {
				sm_bplaSelector.Items.Add($"БПЛА-{i}");
				_userData.Add(new DataUnit());
			}
			sm_bplaSelector.SelectedIndex = _cur = 0;
		}

		/// <summary>
		/// Очередной шаг прорисовки
		/// </summary>
		private void PaintStep() {
			Graphics g = sm_poligon.CreateGraphics(); // создаем место для рисования
			g.TranslateTransform(_lenX/2.0f, _lenY/2.0f); // смещение начала координат (в пикселях)
			g.ScaleTransform(1f,-1f); //Инвертируем оси по "канону"

			Pen gridPen = new Pen(Color.Black, 0.0001f); //перо для отрисовки координатной сетки
			Pen penCO = new Pen(Color.Black, 2f); // Перо для отрисовки осей
			Font fo = new System.Drawing.Font(FontFamily.GenericSerif, 3f); //Грифт для вывода текстовой информации; в данном случае не используется

			//Отрисовка осей и сетки
			g.DrawLine(penCO, new Point(-5000, 0), new Point(5000, 0));
			g.DrawLine(penCO, new Point(0, -5000), new Point(0, 5000));
			for (float i = _sp.X*_scaleX; i < _ep.X*_scaleX + 1; i += _dX)
				g.DrawLine(gridPen, i, _sp.Y, i, _ep.Y);
			for (float i = _sp.Y*_scaleY; i > _ep.Y*_scaleY - 1; i -= _dY)
				g.DrawLine(gridPen, _sp.X, i, _ep.X, i);

			//Отрисовка всех валидных bpla
			if (_bplas == null) return;
			foreach (var bpla in _bplas) {
				var br = new SolidBrush(bpla.Color);
				var pt = new Point(
					(int)Math.Round((bpla.Coord.X - bpla.Radius) * _scaleX),
					(int)Math.Round((bpla.Coord.Y - bpla.Radius) * _scaleY)
				);
				var sz = new Size(
					(int) Math.Round(2 * bpla.Radius),
					(int) Math.Round(2 * bpla.Radius)
				);
				var rect = new Rectangle(pt, sz);
				g.FillEllipse(br, rect);
			}
//			g.DrawCurve(pen, p.ToArray()); // отрисовка графика. Так так метод DrawCurwe() не может считывать данные из листа, а только из массива, то создаем из листа массив, по данным которого и идет постороение графика
		}

		/// <summary>
		/// Перерисовка полигона
		/// </summary>
		private void sm_poligon_Paint(object sender, PaintEventArgs e) {
			PaintStep();//Рисуем
		}

		/// <summary>
		/// Перейти к настройке следующего БПЛА
		/// </summary>
		private void sm_next_Click(object sender, EventArgs e) {
			if (sm_bplaSelector.SelectedIndex < sm_bplaSelector.Items.Count - 1)
				sm_bplaSelector.SelectedIndex++;
		}

		/// <summary>
		/// Перейти к настройке предыдущего БПЛА
		/// </summary>
		private void sm_prev_Click(object sender, EventArgs e) {
			if (sm_bplaSelector.SelectedIndex > 0)
				sm_bplaSelector.SelectedIndex--;
		}

		/// <summary>
		/// Был выбран другой БПЛА
		/// </summary>
		private void sm_bplaSelector_SelectedIndexChanged(object sender, EventArgs e) {
			// Запоминаем текущие настройки во временное хранилище
			_userData[_cur].Radius = sm_radiusImput.Text;
			_userData[_cur].Speed = sm_speedImput.Text;
			_userData[_cur].P1X = sm_p1X.Text;
			_userData[_cur].P1Y = sm_p1Y.Text;
			_userData[_cur].P2X = sm_p2X.Text;
			_userData[_cur].P2Y = sm_p2Y.Text;
			_userData[_cur].P3X = sm_p3X.Text;
			_userData[_cur].P3Y = sm_p3Y.Text;

			// Достаем данные для текущего БПЛА
			sm_radiusImput.Text = _userData[sm_bplaSelector.SelectedIndex].Radius;
			sm_speedImput.Text = _userData[sm_bplaSelector.SelectedIndex].Speed;
			sm_p1X.Text = _userData[sm_bplaSelector.SelectedIndex].P1X;
			sm_p1Y.Text = _userData[sm_bplaSelector.SelectedIndex].P1Y;
			sm_p2X.Text = _userData[sm_bplaSelector.SelectedIndex].P2X;
			sm_p2Y.Text = _userData[sm_bplaSelector.SelectedIndex].P2Y;
			sm_p3X.Text = _userData[sm_bplaSelector.SelectedIndex].P3X;
			sm_p3Y.Text = _userData[sm_bplaSelector.SelectedIndex].P3Y;

			//Меняем текст GroupBox и меняем _cur не текущий
			sm_settingsBox.Text = $@"БПЛА-{sm_bplaSelector.SelectedIndex+1}";
			_cur = sm_bplaSelector.SelectedIndex;
		}

		/// <summary>
		/// Проверить все поля каждого элемента временного хранилища
		/// </summary>
		/// <param name="iData">Временное хранилище</param>
		/// <returns>True - нет ошибок. False - есть ошибка</returns>
		private static Dictionary<int, byte> CheckInputDatas(List<DataUnit> iData) {
			//База ошибок
			var errs = new Dictionary<int, byte>();

			//Проверяем все поля каждого элеента пользовательских данных
			for(var i = 0; i < iData.Count; ++i) {
				CheckDataField(iData[i].Radius, 0.3f, 30f,  ref errs, i, "00000001");
				CheckDataField(iData[i].Speed, 0.5f, 10f,   ref errs, i, "00000010");
				CheckDataField(iData[i].P1X, -1000f, 1000f, ref errs, i, "00000100");
				CheckDataField(iData[i].P1Y, -1000f, 1000f, ref errs, i, "00001000");
				CheckDataField(iData[i].P2X, -1000f, 1000f, ref errs, i, "00010000");
				CheckDataField(iData[i].P2Y, -1000f, 1000f, ref errs, i, "00100000");
				CheckDataField(iData[i].P3X, -1000f, 1000f, ref errs, i, "01000000");
				CheckDataField(iData[i].P3Y, -1000f, 1000f, ref errs, i, "10000000");
			}

			return (errs.Count == 0) ? null : errs;
		}

		private static void PrintErrs(Dictionary<int, byte> errs) {
			// Для всех ошибок выводим соответствующие сообщения
			foreach (var i in errs.Keys) {
				string mess = $"БПЛА-{i + 1}:";

				//Генерация сообщения об ошибке (проверка по маске: в каком поле ошибка, в том бите 1)
				if ((errs[i] & Convert.ToByte("00000001", 2)) != 0)
					mess += "\n * Радиус должен быть числом в диапазоне от 0,3 до 30,0.";

				if ((errs[i] & Convert.ToByte("00000010", 2)) != 0)
					mess += "\n * Скорость должна быть числом в диапазоне от 0,500 до 10,000.";

				if ((errs[i] & Convert.ToByte("11111100", 2)) != 0) {
					mess += "\n * Поля ";
					if ((errs[i] & Convert.ToByte("00000100", 2)) != 0)
						mess += "P1-X; ";
					if ((errs[i] & Convert.ToByte("00001000", 2)) != 0)
						mess += "P1-Y; ";
					if ((errs[i] & Convert.ToByte("00010000", 2)) != 0)
						mess += "P2-X; ";
					if ((errs[i] & Convert.ToByte("00100000", 2)) != 0)
						mess += "P2-Y; ";
					if ((errs[i] & Convert.ToByte("01000000", 2)) != 0)
						mess += "P3-X; ";
					if ((errs[i] & Convert.ToByte("10000000", 2)) != 0)
						mess += "P3-Y; ";
					mess += "должны содержать числа в диапазоне от -1000,000 до 1000,000.";
				}

				//Выводим ошибки для БПЛА
				MessageBox.Show(
					mess,
					$@"Неверные данные: БПЛА-{i + 1}",
					MessageBoxButtons.OK,
					MessageBoxIcon.Warning
				);
			}
		}

		/// <summary>
		/// Проверяет строку на бытие числом и вхождение оного в диапазон указанный. Ошибки заносит в спец. базу.
		/// </summary>
		/// <param name="dfield">Поле для проверки</param>
		/// <param name="min">Минимальное значение</param>
		/// <param name="max">Максимальное значение</param>
		/// <param name="errs">База ошибок</param>
		/// <param name="n">Норме записи в базе</param>
		/// <param name="mask">Код ошибки для записи</param>
		private static void CheckDataField(string dfield, float min, float max, ref Dictionary<int, byte> errs, int n, string mask) {
			try {
				//Пытаемся получить из текста число
				var buff = float.Parse(dfield);
				if(buff > max || buff < min)
					AddError(ref errs, n, mask); //Если не в диапазоне
			} catch (Exception) {
				//Если не число
				AddError(ref errs, n, mask);
			}
		}

		/// <summary>
		/// Отметить ошибку
		/// </summary>
		/// <param name="errs">База ошибок</param>
		/// <param name="n">Номер записи в базе</param>
		/// <param name="mask">Код ошибки</param>
		private static void AddError(ref Dictionary<int, byte> errs, int n, string mask) {
			if (!errs.ContainsKey(n))
				errs[n] = 0; // "00000000"
			errs[n] |= Convert.ToByte(mask, 2);
		}

		/// <summary>
		/// Начинаем/заканчиваем моделирование
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void sm_start_Click(object sender, EventArgs e) {
			//Дозаписываем во временное хранилище
			sm_bplaSelector_SelectedIndexChanged(null, EventArgs.Empty);

			//Проверяем данные во временном хранилище
			var errs = CheckInputDatas(_userData);
			if (errs != null) {
				PrintErrs(errs);
				return;
			}

			MessageBox.Show(
				@"Ошибок не обнаружено!",
				@"Результаты проверки",
				MessageBoxButtons.OK,
				MessageBoxIcon.Information
			);

			//Если все в порядке, создаем на основе этих данных БПЛА
			_bplas = new List<BPLA>();
			foreach (var bplaData in _userData) {
				_bplas.Add(new BPLA(
					new[] {
						//TODO: Подумать, что делать. Как будут соотноситься вещественные значения с целочисленными
						new BpPoint(float.Parse(bplaData.P1X), float.Parse(bplaData.P1Y)),
						new BpPoint(float.Parse(bplaData.P2X), float.Parse(bplaData.P2Y)),
						new BpPoint(float.Parse(bplaData.P3X), float.Parse(bplaData.P3Y)),
					},
					float.Parse(bplaData.Speed),
					float.Parse(bplaData.Radius)
				));
			}

			//Моделирование
			sm_poligon.Invalidate();
		}
	}

	internal class DataUnit {
		public string Radius, Speed, P1X, P1Y, P2X, P2Y, P3X, P3Y;

		public DataUnit() {
			Radius = Speed = P1X = P1Y = P2X = P2Y = P3X = P3Y = "";
		}
	};

}
